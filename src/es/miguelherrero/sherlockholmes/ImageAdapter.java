package es.miguelherrero.sherlockholmes;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import es.miguelherrero.sherlockholmes.content.Data;

public class ImageAdapter extends BaseAdapter {
	private Context mContext;

	public ImageAdapter(Context c) {
		mContext = c;
	}

	@Override
	public int getCount() {
		return Data.characters.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View grid;

		if (convertView == null) { // if it's not recycled, initialize some
			// attributes
			grid = new View(mContext);
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			grid = inflater.inflate(R.layout.mygrid, parent, false);
		} else {
			grid = convertView;
		}

		ImageView imageView = (ImageView) grid.findViewById(R.id.imagepart);
		TextView textView = (TextView) grid.findViewById(R.id.textpart);

		// retrieve resources IDs from Data array
		int text_id = mContext.getResources().getIdentifier(Data.characters[position][0] + "_title", "string", mContext.getPackageName());
		textView.setText(text_id);

		int image_id = mContext.getResources().getIdentifier(Data.characters[position][1], "drawable", mContext.getPackageName());
		imageView.setImageResource(image_id);

		return grid;
	}
}
