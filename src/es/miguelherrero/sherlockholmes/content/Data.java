package es.miguelherrero.sherlockholmes.content;

public class Data {
	public static String[][] characters = {
		{"holmes", "f_holmes"},
		{"adler", "f_adler"},
		{"moriarty", "f_moriarty"},
		{"mycroft", "f_mycroft"},
		{"watson", "f_watson"},
		{"winter", "f_winter"}
	};
}
