package es.miguelherrero.sherlockholmes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import es.miguelherrero.sherlockholmes.content.Data;

public class DetailActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		// Show the Up button in the action bar.
		setupActionBar();

		final TextView title = (TextView) findViewById(R.id.detail_title);
		final ImageView image = (ImageView) findViewById(R.id.detail_image);
		final ScrollView text = (ScrollView) findViewById(R.id.detail_text);

		// Cogemos los datos del Bundle
		Bundle b = getIntent().getExtras();
		final int position = b.getInt("position");

		// Título
		int title_id = getResources().getIdentifier(Data.characters[position][0] + "_title", "string", getPackageName());
		title.setText(title_id);

		// Imagen
		int id = getResources().getIdentifier(Data.characters[position][1], "drawable", getPackageName());
		image.setImageResource(id);

		// Texto
		int text_id = getResources().getIdentifier(Data.characters[position][0] + "_text", "string", getPackageName());
		TextView tv1 = new TextView(this);
		tv1.setText(text_id);
		text.addView(tv1);

	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				// This ID represents the Home or Up button. In the case of this
				// activity, the Up button is shown. Use NavUtils to allow users
				// to navigate up one level in the application structure. For
				// more details, see the Navigation pattern on Android Design:
				//
				// http://developer.android.com/design/patterns/navigation.html#up-vs-back
				//
				NavUtils.navigateUpFromSameTask(this);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
